package com.capgemini.spring;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ServiceTestSuite.class, RepositoryTestSuite.class, ControllerTestSuite.class })
public class AllTests {

}
