package com.capgemini.spring;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.capgemini.spring.controller.BookControllerTest;
import com.capgemini.spring.mvc.controller.BookController;

@RunWith(Suite.class)
@SuiteClasses({ BookControllerTest.class })
public class ControllerTestSuite {

}
