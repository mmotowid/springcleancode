package com.capgemini.spring.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.capgemini.spring.dataaccess.enumeration.BookStatus;
import com.capgemini.spring.dataaccess.to.BookTo;
import com.capgemini.spring.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class BookRestServiceTest {

	@Mock
	private BookService mBookService;

	@InjectMocks
	@Autowired
	private BookRestService mSut;

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		Mockito.reset(mBookService);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	/**
	 * Tests method with declaration below
	 * 
	 * @see @RequestMapping(value = "/getAllBooks", method = RequestMethod.GET,
	 *      produces = MediaType.APPLICATION_JSON_VALUE)
	 */
	@Test
	public void testGetAllBooks() throws Exception {
		// given
		List<BookTo> expectedBookList = prepareDummyBookList(1);
		BookTo book = expectedBookList.get(0);

		Mockito.when(mBookService.findAllBooks()).thenReturn(expectedBookList);
		// when
		ResultActions resultActions = mockMvc.perform(get("/rest/getAllBooks"));

		// then
		resultActions//
				.andExpect(status().isOk())//
				.andExpect(jsonPath("[0].id").value(book.getId().intValue()))
				.andExpect(jsonPath("[0].title").value(book.getTitle()))
				.andExpect(jsonPath("[0].authors").value(book.getAuthors()));

	}

	/**
	 * Tests method with declaration below
	 * 
	 * @see @RequestMapping(value = "/createBook", // method =
	 *      RequestMethod.POST, // consumes = MediaType.APPLICATION_JSON_VALUE,
	 *      // produces = MediaType.APPLICATION_JSON_VALUE)
	 */
	@Test
	public void testCreateABook() throws Exception {
		// given
		BookTo book = new BookTo(10l, "title", "author", BookStatus.FREE);

		Mockito.when(mBookService.saveBook(Mockito.any(BookTo.class))).thenReturn(book);
		// when
		ResultActions resultActions = mockMvc.perform(post("/rest/createBook").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(book)));
		// then
		resultActions.andExpect(status().isOk());

	}

	// private
	private List<BookTo> prepareDummyBookList(int howMany) {
		List<BookTo> result = new ArrayList<BookTo>(howMany);
		for (int i = 0; i < howMany; i++) {
			result.add(new BookTo(new Long(i), "title " + i, "author " + i, BookStatus.FREE));
		}
		return result;
	}

	/**
	 * (Example) Sample method which convert's any object from Java to String
	 */
	private static String asJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(obj);
			return jsonContent;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
