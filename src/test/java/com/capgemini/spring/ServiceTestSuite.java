package com.capgemini.spring;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.capgemini.spring.rest.BookRestService;
import com.capgemini.spring.rest.BookRestServiceTest;

@RunWith(Suite.class)
@SuiteClasses({ BookRestServiceTest.class })
public class ServiceTestSuite {

}
