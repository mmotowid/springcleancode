package com.capgemini.spring.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.capgemini.spring.dataaccess.enumeration.BookStatus;
import com.capgemini.spring.dataaccess.to.BookTo;
import com.capgemini.spring.mvc.ModelConstants;
import com.capgemini.spring.mvc.controller.BookController;
import com.capgemini.spring.service.BookService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class BookControllerTest {
	
	@Mock
	private BookService bookService;

	@InjectMocks
	private BookController mSUT;
	
	private MockMvc mockMvc;

	@Before
	public void setup() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".html");
		mockMvc = MockMvcBuilders.standaloneSetup(mSUT).setViewResolvers(viewResolver).build();
	}

	@Test
	public void testGetAllBooks() throws Exception {
//		given 
		List<BookTo> expectedBookList = new ArrayList<>();
		BookTo singleBook = new BookTo(1l, "title", "author", BookStatus.MISSING);
		expectedBookList.add(singleBook);
		// when
		ResultActions resultActions = mockMvc.perform(get("/books/all"));
		// then
		resultActions.andExpect(view().name("books"));
		resultActions.andExpect(getBookListMatcher(expectedBookList));
	}

	private ResultMatcher getBookListMatcher(List<BookTo> expectedBookList) {
		return new ResultMatcher() {
			@SuppressWarnings("unchecked")
			@Override
			public void match(MvcResult result) throws Exception {
				List<BookTo> retrievedBooks = (List<BookTo>) result.getModelAndView().getModel()
						.get(ModelConstants.BOOK_LIST);

				// TODO: Implement some test logic
				for (BookTo singleBook : expectedBookList) {

				}

			}
		};
	}

	@Test
	public void testBookDetails() throws Exception {
		// given when
		ResultActions resultActions = mockMvc.perform(get("/books/book").param("id", "1"));
		// then
		resultActions.andExpect(view().name("book"));
	}

	@Test
	public void testRemoveBook() throws Exception {
		// given when
		ResultActions resultActions = mockMvc.perform(get("/books/remove").param("id", "1"));
		// then
		resultActions.andExpect(view().name("books"));
	}
}
