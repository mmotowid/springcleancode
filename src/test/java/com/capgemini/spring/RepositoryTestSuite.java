package com.capgemini.spring;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.capgemini.spring.repository.BookRepositoryTestStep;

@RunWith(Suite.class)
@SuiteClasses({ BookRepositoryTestStep.class })
public class RepositoryTestSuite {

}
