package com.capgemini.spring.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.spring.dataaccess.entities.BookEntity;
import com.capgemini.spring.dataaccess.repository.BookRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class BookRepositoryTestStep {

	@Autowired
	private BookRepository mBookRepository;

	@Test
	@Transactional
	public void testReadAllBooks() throws Exception {

		//given:
		//when:
		List<BookEntity> books = mBookRepository.findAll();
		// then:
		Assert.assertNotNull(books);

	}
	
	@Test
	@Transactional
	public void testFindBookById() throws Exception {
		//given:
		long  bookId = 1l;
		//when:
		BookEntity book = mBookRepository.findOne(bookId);
		// then:
		Assert.assertNotNull(book);
		Assert.assertEquals("Jan Kowalski", book.getAuthors());
	}

}
