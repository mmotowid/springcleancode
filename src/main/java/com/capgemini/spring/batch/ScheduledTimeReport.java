package com.capgemini.spring.batch;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTimeReport {

	private static final Logger LOG = Logger.getLogger(ScheduledTimeReport.class.getName());

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() {
		LOG.info("Current time: " + dateFormat.format(new Date()));
	}
}