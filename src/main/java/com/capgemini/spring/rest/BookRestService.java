package com.capgemini.spring.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.spring.dataaccess.enumeration.BookStatus;
import com.capgemini.spring.dataaccess.to.BookTo;
import com.capgemini.spring.service.BookService;

/**
 * {@link BookTo} rest controller
 * 
 * @author mmotowid
 *
 */
@RestController
@RequestMapping("/rest")
public class BookRestService {

	@Autowired
	private BookService mBookService;

	/**
	 * Simple GET method
	 */
	@RequestMapping(value = "/getDummyBook", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BookTo getBook() {
		BookTo dummyBook = new BookTo(1L, "title", "author", BookStatus.FREE);
		return dummyBook;
	}

	/**
	 * Simple POST method
	 */
	@RequestMapping(value = "/createBook", //
			method = RequestMethod.POST, //
			consumes = MediaType.APPLICATION_JSON_VALUE, //
			produces = MediaType.APPLICATION_JSON_VALUE)
	public BookTo createBook(@RequestBody BookTo book) {
		return mBookService.saveBook(book);
	}

	/**
	 * Simple GET method
	 */
	@RequestMapping(value = "/deleteBook/{bookId}", //
			method = RequestMethod.DELETE)
	public boolean deleteBook(@PathVariable("bookId") Long bookId) {
		mBookService.deleteBook(bookId);
		return true;
	}

	/**
	 * Simple GET method
	 */
	@RequestMapping(value = "/getAllBooks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<BookTo> getAllBooks() {
		return mBookService.findAllBooks();
	}
}