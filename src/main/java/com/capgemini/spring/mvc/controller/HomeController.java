package com.capgemini.spring.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.capgemini.spring.mvc.ModelConstants;
import com.capgemini.spring.mvc.ViewNames;

/**
 * Home controller impl
 * 
 * @author mmotowid
 *
 */
@Controller
public class HomeController {

	private static final String INFO_TEXT = "Here You shall display information containing informations about newly created TO";
	private static final String WELCOME = "This is a welcome page";

	@RequestMapping("/")
	public String welcome(Model model) {
		model.addAttribute(ModelConstants.GREETING, WELCOME);
		model.addAttribute(ModelConstants.INFO, INFO_TEXT);
		return ViewNames.WELCOME;
	}
}
