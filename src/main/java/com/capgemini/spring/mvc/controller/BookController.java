package com.capgemini.spring.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.spring.dataaccess.to.BookTo;
import com.capgemini.spring.mvc.ModelConstants;
import com.capgemini.spring.mvc.ViewNames;
import com.capgemini.spring.service.BookService;

/**
 * Book controller
 * 
 * @author mmotowid
 *
 */
@Controller
@RequestMapping("/books")
public class BookController {

	@Autowired
	private BookService bookService;

	private static final String WAS_REMOVED_FROM_BOOK_LIST = " was removed from book list";
	private static final String BOOK_OF_ID = "Book of id: ";
	private static final String info = "Info about books";

	/**
	 * Get list of books
	 */
	@RequestMapping
	public ModelAndView list() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(ModelConstants.BOOK_LIST, bookService.findAllBooks());
		modelAndView.addObject(ModelConstants.INFO, info);
		modelAndView.setViewName(ViewNames.BOOKS);
		return modelAndView;
	}

	/**
	 * Same method as above but annotated with path
	 */
	@RequestMapping(value = "/all")
	public ModelAndView allBooks() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(ModelConstants.BOOK_LIST, bookService.findAllBooks());
		modelAndView.addObject(ModelConstants.INFO, info);
		modelAndView.setViewName(ViewNames.BOOKS);
		return modelAndView;
	}

	/**
	 * Single book display
	 * 
	 * @param model
	 *            {@link Model}
	 * @param id
	 *            {@link RequestParam} ID
	 * @return view name
	 */
	@RequestMapping(value = "/book")
	public String details(Model model, @RequestParam("id") Long id) {
		model.addAttribute(ModelConstants.BOOK, bookService.findBookById(id));
		return ViewNames.BOOK;
	}

	/**
	 * Single book remove
	 * 
	 * @param model
	 *            {@link Model}
	 * @param id
	 *            {@link RequestParam} ID
	 * @return view name
	 */
	@RequestMapping(value = "/remove")
	public String remove(Model model, @RequestParam("id") Long id) {
		bookService.deleteBook(id);
		model.addAttribute(ModelConstants.BOOK_LIST, bookService.findAllBooks());
		String message = BOOK_OF_ID + id + WAS_REMOVED_FROM_BOOK_LIST;
		model.addAttribute(ModelConstants.INFO, message);
		return ViewNames.BOOKS;
	}

	/**
	 * Get search view
	 */
	@RequestMapping(value = "/search")
	public String search() {
		return ViewNames.SEARCH;
	}

	/**
	 * Redirects page to page on which adding new book is available
	 * 
	 * @return model with no data inside and logical name of page to display
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView showAddBookView() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(ModelConstants.NEW_BOOK, new BookTo());
		modelAndView.setViewName(ViewNames.ADD_BOOK);
		return modelAndView;
	}

	/**
	 * Adds new Book
	 * 
	 * @param newBook
	 *            object tied with formular and data from formular inside it
	 * @return modelAndView with new BookTo object to be tied with formular on
	 *         view that will be displayed
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addBook(@ModelAttribute(ModelConstants.NEW_BOOK) BookTo newBook) {
		bookService.saveBook(newBook);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ViewNames.ADD_BOOK);
		modelAndView.addObject(ModelConstants.NEW_BOOK, new BookTo());
		return modelAndView;
	}

	/**
	 * Data binder for JSP model attributes
	 */
	@InitBinder
	public void initialiseBinder(WebDataBinder binder) {
		binder.setAllowedFields("id", "title", "authors", "status");
	}

}
