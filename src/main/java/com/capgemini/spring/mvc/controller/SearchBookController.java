package com.capgemini.spring.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.capgemini.spring.mvc.ModelConstants;
import com.capgemini.spring.mvc.ViewNames;
import com.capgemini.spring.service.BookService;

/**
 * Search book controller
 * 
 * @author mmotowid
 *
 */
@Controller
@RequestMapping("/search")
public class SearchBookController {

	@Autowired
	private BookService bookService;

	@RequestMapping(value = "/result")
	public String details(Model model, @RequestParam("title") String title, @RequestParam("author") String author) {
		model.addAttribute(ModelConstants.BOOK_LIST, bookService.findBooksByTitleAndAuthor(title, author));
		return ViewNames.BOOKS;
	}

	@InitBinder
	public void initialiseBinder(WebDataBinder binder) {
		binder.setAllowedFields("id", "title", "authors", "status");
	}

}
