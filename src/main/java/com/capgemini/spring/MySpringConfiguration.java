package com.capgemini.spring;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * App configuration
 * 
 * @author mmotowid
 *
 */
@Configuration
@EnableAspectJAutoProxy
@EnableScheduling
@EntityScan("com.capgemini.spring.dataaccess.entities")
public class MySpringConfiguration {

}
