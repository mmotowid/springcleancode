package com.capgemini.spring.aspect;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.capgemini.spring.dataaccess.to.BookTo;
import com.capgemini.spring.service.BookService;

/**
 * Not very usefull aspect for book generation
 * 
 * @author mmotowid
 *
 */
@Aspect
@Component
public class BookIdGeneratorAspect {

	private static final Logger LOG = Logger.getLogger(BookIdGeneratorAspect.class.getName());

	/**
	 * Pointcut which defines method on which invocation aspect will be called
	 */
	// TODO: remove comment for Aspect presentation
	// @Before("execution(*
	// com.capgemini.spring.service.BookService.saveBook(..))")
	public void generateId(JoinPoint joinPoint) {

		LOG.info("Book ID generator aspect");

		List<Object> arguments = Arrays.asList(joinPoint.getArgs());
		if (arguments != null && arguments.size() > 0) {
			Object firstArg = arguments.iterator().next();
			if (firstArg instanceof BookTo) {
				BookTo bookTo = (BookTo) firstArg;
				if (null == bookTo.getId()) {
					bookTo.setId(ThreadLocalRandom.current().nextLong());
				}
			}
		}
	}

	/**
	 * Pointcut which defines method on which invocation aspect will be called
	 */
	@After("execution(* com.capgemini.spring.service.BookService.*(..))")
	public void postprocessing(JoinPoint joinPoint) {
		LOG.info("Method called after ANY method from " + BookService.class.getName());
	}
}
