package com.capgemini.spring.dataaccess.enumeration;

/**
 * Book status enumeration
 * 
 * @author mmotowid
 *
 */
public enum BookStatus {

	FREE, LOAN, MISSING
}
