package com.capgemini.spring.service;

import java.util.List;

import com.capgemini.spring.dataaccess.to.BookTo;

/**
 * Service API
 * 
 * @author mmotowid
 *
 */
public interface BookService {

	public List<BookTo> findAllBooks();

	public List<BookTo> findBooksByTitle(String title);

	public List<BookTo> findBooksByAuthor(String author);

	public List<BookTo> findBooksByTitleAndAuthor(String title, String author);

	public BookTo saveBook(BookTo book);

	public void deleteBook(Long id);

	public BookTo findBookById(Long id);
}
