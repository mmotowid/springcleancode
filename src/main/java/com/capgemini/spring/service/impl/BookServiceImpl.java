package com.capgemini.spring.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.dataaccess.entities.BookEntity;
import com.capgemini.spring.dataaccess.repository.BookRepository;
import com.capgemini.spring.dataaccess.to.BookTo;
import com.capgemini.spring.service.BookService;
import com.capgemini.spring.service.mapper.BookMapper;

/**
 * Service impl
 * 
 * @author mmotowid
 *
 */
@Service
@Transactional(readOnly = true)
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository bookRepository;

	@Override
	public List<BookTo> findAllBooks() {
		return BookMapper.map2To(bookRepository.findAll());
	}

	@Override
	public List<BookTo> findBooksByTitle(String title) {
		return BookMapper.map2To(bookRepository.findBookByTitle(title));
	}

	@Override
	public List<BookTo> findBooksByAuthor(String author) {
		return BookMapper.map2To(bookRepository.findBookByAuthor(author));
	}

	@Override
	@Transactional(readOnly = false)
	public BookTo saveBook(BookTo book) {
		BookEntity entity = BookMapper.map(book);
		entity = bookRepository.save(entity);
		return BookMapper.map(entity);
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteBook(Long id) {
		bookRepository.delete(id);
	}

	@Override
	public BookTo findBookById(Long id) {
		List<BookTo> books = findAllBooks();
		for (BookTo book : books) {
			if (book.getId() == id) {
				return book;
			}
		}
		return null;
	}

	@Override
	public List<BookTo> findBooksByTitleAndAuthor(String title, String author) {
		List<BookTo> booksByTitle = new ArrayList<>();
		List<BookTo> booksByAuthor = new ArrayList<>();

		if (null != title && title.length() > 0) {
			booksByTitle = findBooksByTitle(title);
		}
		if (null != author && author.length() > 0) {
			booksByAuthor = findBooksByAuthor(author);
		}

		for (BookTo to : booksByAuthor) {
			if (!booksByTitle.contains(to)) {
				booksByTitle.add(to);
			}
		}
		return booksByTitle;
	}
}
