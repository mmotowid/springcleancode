<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>Books</title>
</head>
<body>
	<section>
		<div class="jumbotron">
			<div class="container">
				<h1>Books Search</h1>
				<p>This page allows to search books based on title or author</p>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
				<div class="thumbnail">
					<div class="caption">
						<form method="get" action="/search/result">
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder="Title" name='title'
										type="text">
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Author" name='author'
										type="text">
								</div>
								<div class="pull-right">
									<button type="submit" class="btn btn-default">
										<span class="glyphicon glyphicon-search" /></span>Search
									</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<div class="col-md-5">
				<p>
					<a href="<spring:url value="/" />" class="btn btn-default"> <span
						class="glyphicon-hand-left glyphicon"></span> Back
					</a>
				</p>
			</div>
		</div>
	</section>

</body>
</html>
