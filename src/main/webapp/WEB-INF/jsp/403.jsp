<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>Error 403</title>
</head>
<body>
	<section>
		<div class="jumbotron">
			<div class="container">
				<h1>Error: Access Denied 403</h1>
				<p>
					<strong>${userName}</strong>${userError}</p>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">
			<h3>${error}</h3>
		</div>
	</section>

</body>
</html>
